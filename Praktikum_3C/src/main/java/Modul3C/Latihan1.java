package Modul3C;
import java.awt.*;
import javax.swing.*;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
public class Latihan1 extends JFrame {
    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 200;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;
public static void main(String[] args) {
 new Latihan1();
    }
public Latihan1(){
    JMenu File,Edit;
    Container contentPane;
    JFrame frame = new JFrame ("Frame Pertama");
    JMenuBar mb= new JMenuBar();
    File = new JMenu ("File");
    Edit = new JMenu ("Edit");
    contentPane = getContentPane();
    contentPane.setBackground(Color.pink);
    contentPane.setLayout(new FlowLayout());
    mb.add(File);
    mb.add(Edit);
    frame.setJMenuBar (mb);
    frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
    frame.setContentPane(contentPane);
    frame.setVisible(true);
    setDefaultCloseOperation(EXIT_ON_CLOSE);
}
}
