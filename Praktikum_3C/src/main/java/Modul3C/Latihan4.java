package Modul3C;
import java.awt.Container;
import javax.swing.*;
public class Latihan4 extends JDialog{
    private static final int BUTTON_WIDTH =90;
    private static final int BUTTON_HEIGHT =40;
         public Latihan4(){
         Container contentPane; 
         contentPane = getContentPane();
        contentPane.setLayout(null);
        this.setSize(600,400);
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.setTitle("CheckBoxDemo ");
        this.setVisible(true);
        JTextArea tulis = new JTextArea("Welcome to Java");
        tulis.setBounds(180, 100, 140, 40);
        contentPane.add(tulis);
        JButton leftButton = new JButton("Left");
        leftButton.setBounds(170, 200, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(leftButton);
        JButton rightButton=new JButton("Right");
        rightButton.setBounds(300, 200, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(rightButton);
        JCheckBox box1 = new JCheckBox("Centered");
        box1.setBounds(110, 50, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(box1);
        JCheckBox box2 = new JCheckBox("Bold");
        box2.setBounds(150, 75, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(box2);
       JCheckBox box3= new JCheckBox("Italic");
        box3.setBounds(3250, 100, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(box3);
    }
         public static void main(String[] args) {
        new Latihan4();
         }
}
