package Modul3C;
import java.awt.Container;
import javax.swing.*;
public class Latihan5 extends JDialog{
      private static final int BUTTON_WIDTH =80;
    private static final int BUTTON_HEIGHT =40;
    public Latihan5(){
        setLayout(null);
         Container contentPane; 
         contentPane = getContentPane();
        this.setSize(700,600);
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.setTitle("RadioButton Demo");
        this.setVisible(true);
        JTextArea tulisan = new JTextArea("Welcome to Java");
        tulisan.setBounds(180, 150, 140, 40);
        this.add(tulisan);
        JButton leftButton = new JButton("Left");
        leftButton.setBounds(120, 60, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(leftButton);
        JButton rightButton=new JButton("Right");
        rightButton.setBounds(350, 100, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(rightButton);
        JCheckBox box1 = new JCheckBox("Centered");
        box1.setBounds(120, 70, BUTTON_WIDTH, BUTTON_HEIGHT);
        this.add(box1);
       JCheckBox box2 = new JCheckBox("Bold");
        box2.setBounds(50, 95, BUTTON_WIDTH, BUTTON_HEIGHT);
        this.add(box2);
       JCheckBox box3= new JCheckBox("Italic");
        box3.setBounds(70, 120, BUTTON_WIDTH, BUTTON_HEIGHT);
        this.add(box3);
        JRadioButton radio1 = new JRadioButton("Red");
        radio1.setBounds(190, 70, BUTTON_WIDTH, BUTTON_HEIGHT);
        this.add(radio1);
        JRadioButton radio2 = new JRadioButton("Green");
        radio2.setBounds(5, 95, BUTTON_WIDTH, BUTTON_HEIGHT);
        this.add(radio2);
         JRadioButton radio3 = new JRadioButton("Blue");
        radio3.setBounds(100, 120, BUTTON_WIDTH, BUTTON_HEIGHT);
        this.add(radio3);
    }
    public static void main(String[] args) {
        new Latihan5();
}
}