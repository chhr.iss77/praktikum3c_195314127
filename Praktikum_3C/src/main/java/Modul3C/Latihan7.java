package Modul3C;
import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.*;
public class Latihan7  extends JDialog {
   public Latihan7(){
       String[] negara = {"Canada","China","Denmark","France","Germany","India","Norway","United Kingdom","United States of America"};
         Container contentPane; 
         contentPane = getContentPane();
         contentPane.setLayout(null);
         this.setSize(700,600);
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.setTitle("List Demo ");
        this.setVisible(true);
        ImageIcon foto = new ImageIcon("australia.png");
        JLabel image1 = new JLabel();
        image1.setIcon(foto);
        image1.setBounds(300,70,200,100);
        contentPane.add(image1);
        ImageIcon foto2 = new ImageIcon("indonesia.png");
        image1 = new JLabel();
        image1.setIcon(foto2);
        image1.setBounds(120,20,200,60);
        contentPane.add(image1);
        ImageIcon foto3 = new ImageIcon("brazil.png");
        image1 = new JLabel();
        image1.setIcon(foto3);
        image1.setBounds(100,20,200,60);
        contentPane.add(image1);
        ImageIcon foto4 = new ImageIcon("malaysia.png");
        image1 = new JLabel();
        image1.setIcon(foto4);
        image1.setBounds(200,70,200,100);
        contentPane.add(image1);
        ImageIcon foto5 = new ImageIcon("jamaica.png");
        image1 = new JLabel();
        image1.setIcon(foto5);
        image1.setBounds(300,70,200,100);
        contentPane.add(image1);
        ImageIcon foto6 = new ImageIcon("USA.png");
        image1 = new JLabel();
        image1.setIcon(foto6);
        image1.setBounds(300,70,200,160);
        contentPane.add(image1);
      JPanel  panel = new JPanel(new GridLayout(0, 1));
       JList list = new JList(negara);
        panel.add(new JScrollPane(list));
        panel.setBounds(2,2,150,200);
        contentPane.add(panel);
   }
    public static void main(String[] args) {
        new Latihan7();
    }
   
    
}