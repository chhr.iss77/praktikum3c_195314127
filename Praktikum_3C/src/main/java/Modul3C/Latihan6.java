package Modul3C;
import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.*;
public class Latihan6 extends JDialog{
    public Latihan6(){
        Container contentPane; 
         contentPane = getContentPane();
        this.setSize(500,300);
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.setTitle("ComboBoxDemo ");
        this.setVisible(true);
        String [] negara = {"Canada",
        "Kanada menjadi salah satu negara dengan keragaman etnis dan multikultural yang paling beragam di dunia."
         ," Imigran di negara ini tergolong besar, jika dibandingkan negara lainnya. Selain itu, "
         , "perekonomian Kanada menjadi terbesar kesepuluh di dunia. Sumber daya alam (yang berlimpah), "
         , "dan jaringan perdagangan internasional menjadi tumpuan utama ekonomi negara ini. "
         , "Hubungan yang panjang dengan Amerika Serikat memberikan dampak yang signifikan terhadap ekonomi dan budaya kedua Negara."};
        ImageIcon canada = new ImageIcon("canada.png");
       JLabel image = new JLabel();
        image.setIcon(canada);
        image.setBounds(30,60,200,150);
        contentPane.add(image);
       JComboBox box = new JComboBox();
        box.addItem("Canada");
        box.setBounds(2,2,484,30);
        contentPane.add(box);
        JLabel text = new JLabel("Canada");
        text.setBounds(85, 100, 100, 160);
        contentPane.add(text);
        JPanel panel = new JPanel(new GridLayout(0, 1));
        JList list = new JList(negara);
        panel.add(new JScrollPane(list));
        panel.setBounds(260,60,230,160);
        contentPane.add(panel);
    }
    public static void main(String[] args) {
        new Latihan6();
    }
}

