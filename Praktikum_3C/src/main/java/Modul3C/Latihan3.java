package Modul3C;
import java.awt.*;
import javax.swing.*;
public class Latihan3 extends JDialog {
public Latihan3(){
    this.setTitle("Text And Icon Label");
    this.setSize(300,200);
    this.setLocation(150,250);
    this.setLayout(new FlowLayout());
    setDefaultCloseOperation(HIDE_ON_CLOSE);
    Container contentPane = getContentPane();
    contentPane.setBackground(Color.WHITE);
    JLabel imgLabel = new JLabel (new ImageIcon("download.png"),JLabel.RIGHT);
    JLabel text= new JLabel ("SUPERMEN");
    text.setBounds(10,30,200,200);
    contentPane.add(imgLabel);
    contentPane.add(text).setForeground(Color.BLUE);   
}
    public static void main(String[] args) {
        Latihan3 gambar = new Latihan3();
        gambar.setVisible(true);
    }
}
