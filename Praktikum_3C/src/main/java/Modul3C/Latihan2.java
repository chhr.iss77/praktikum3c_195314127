package Modul3C;
import java.awt.Container;
import javax.swing.*;
public class Latihan2 extends JDialog { 
    private static final int FRAME_WIDTH = 500;
    private static final int FRAME_HEIGHT= 500;
    public static void main(String[] args) {
        Latihan2 dialog = new Latihan2();
        dialog.setVisible(true);
    }
    public Latihan2() {
        setSize(FRAME_WIDTH,FRAME_HEIGHT);
        setTitle ("ButtonTest");
        Container contentPane;
        JButton YELLOW,BLUE,RED;
        setLayout(null);
        contentPane = getContentPane();
        YELLOW = new JButton("YELLOW");
        YELLOW.setBounds(0,0,100,50);
        BLUE = new JButton("BLUE");
        BLUE.setBounds(100,0,100,50);
        RED = new JButton("RED");
        RED.setBounds(200,0,100,50);
        contentPane.add(YELLOW);
        contentPane.add(BLUE);
        contentPane.add(RED);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    }
}
